insert into role(name) values ('ROLE_DIRECTOR'),('ROLE_HR_MANAGER'),('ROLE_MANAGER'),('ROLE_WORKER');

insert into usr( id,first_name,last_name,account_non_expired, account_non_locked, created_at, credentials_non_expired, email, email_code, enabled, password, updated_at)
values ('a215434a-01bc-11ec-9a03-0242ac130003',
        'Boshliq',
        'Boshliqov',
        true,
        true,
        now(),
        true,
        'root@mail.ru',
        null,
        true,
        '$2a$10$eoMcclSxxvWldc215xnC8e0jIDqAa17Rd/15w/MRn2yo0/P3c1YrG', --parol=123
        null );

insert into usr_roles(usr_id, roles_id) VALUES ('a215434a-01bc-11ec-9a03-0242ac130003',1)
