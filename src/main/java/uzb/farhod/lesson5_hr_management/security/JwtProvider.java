package uzb.farhod.lesson5_hr_management.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;
import uzb.farhod.lesson5_hr_management.entity.Role;

import java.util.Date;
import java.util.Set;

@Component
public class JwtProvider {
    private String secretKey="juda maxfiy kalit";
    private long expireTime=1000*60*60*24*2;

    public String generateToken(String username, Set<Role> roles){
        String token = Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + expireTime))
                .claim("roles", roles)
                .compact();
        return token;
    }

    public String getEmailFromToken(String token){
        try {
            String email = Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();

            return email;
        }catch (Exception e){
            return null;
        }

    }
}
