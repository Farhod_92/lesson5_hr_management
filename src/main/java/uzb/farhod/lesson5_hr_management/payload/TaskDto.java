package uzb.farhod.lesson5_hr_management.payload;

import lombok.Getter;
import uzb.farhod.lesson5_hr_management.entity.User;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Getter
public class TaskDto {
    @NotNull(message = "task nomi bo'sh")
    private String name;

    private String annotation;

    private Date deadLine;//tugatish vaqti

    @NotNull(message = "user id bo'sh")
    private UUID userId;
}
