package uzb.farhod.lesson5_hr_management.payload;

import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
public class RegisterDto {
    @NotNull(message = "ism bo'sh")
    @Size(min = 3, max = 50, message = "ism 3 dan katta 50 dan kichik bo'lishi kerak")
    private String firstName;

    @NotNull(message = "familiya bo'sh")
    @Size(min = 3, max = 50, message = "lastname 3 dan katta 50 dan kichik bo'lishi kerak")
    private String lastName;

    @Email(message = "bu email emas")
    @NotNull(message = "emailda bo'sh")
    private String email;

    @NotNull(message = "salary bo'sh")
    private Float salary;

    @NotNull(message = "role nomi bo'sh")
    private String roleName;
}
