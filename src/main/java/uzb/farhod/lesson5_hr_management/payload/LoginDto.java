package uzb.farhod.lesson5_hr_management.payload;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
public class LoginDto {
    @NotNull
    @Email
    private String username;//username sifatida ketadi

    @NotNull
    private String password;
}
