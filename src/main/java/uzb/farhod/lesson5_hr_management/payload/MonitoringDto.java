package uzb.farhod.lesson5_hr_management.payload;

import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
public class MonitoringDto {
    @NotNull
    private UUID userId;

    private Timestamp fromTime;

    private Timestamp untilTime;
}
