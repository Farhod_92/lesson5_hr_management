package uzb.farhod.lesson5_hr_management.payload;

import lombok.Getter;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
public class TurniketDto {
    @NotNull
    private UUID userId;

    @NotNull
    private boolean come;

}
