package uzb.farhod.lesson5_hr_management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uzb.farhod.lesson5_hr_management.entity.Turniket;
import uzb.farhod.lesson5_hr_management.entity.User;
import uzb.farhod.lesson5_hr_management.payload.TurniketDto;
import uzb.farhod.lesson5_hr_management.repository.TurniketRepository;
import uzb.farhod.lesson5_hr_management.repository.UserRepository;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("api/turniket")
public class TurniketController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TurniketRepository turniketRepository;

    @PostMapping
    public ResponseEntity<?> add(@Valid @RequestBody TurniketDto turniketDto){
        Optional<User> optionalUser = userRepository.findById(turniketDto.getUserId());
        if(!optionalUser.isPresent())
            return ResponseEntity.badRequest().build();

        User user= optionalUser.get();

        Turniket turniket=new Turniket();
        turniket.setUser(user);
        turniket.setCome(turniketDto.isCome());
        turniketRepository.save(turniket);
        return ResponseEntity.status(201).body("saved");
    }
}
