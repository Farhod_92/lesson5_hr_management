package uzb.farhod.lesson5_hr_management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson5_hr_management.entity.Salary;
import uzb.farhod.lesson5_hr_management.payload.ApiResponse;
import uzb.farhod.lesson5_hr_management.service.SalaryService;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@RestController
@PreAuthorize(value = "hasAnyRole('ROLE_DIRECTOR', 'ROLE_HR_MANAGER')")
@RequestMapping("/api/salary")
public class SalaryController {

    @Autowired
    private SalaryService salaryService;

    @PostMapping("/{userId}")
    public ResponseEntity<?> payment(@PathVariable UUID userId){
        ApiResponse payment = salaryService.payment(userId);
        return ResponseEntity.status(payment.isSuccess()?200:409).body(payment);
    }

    //oy bo'yicha berilgan oyliklarni ko'rish
    //2021-01 formatda oy keladi
    @GetMapping("/month/{time}")
    public ResponseEntity<?> getPaymentsByMonth(@PathVariable String time){
        List<Salary> salaryList = salaryService.getPaymentsByMonth(time);
        return ResponseEntity.ok(salaryList);
    }

    //xodimga berilgan oyliklarni ko'rish
    @GetMapping("/user/{userId}")
    public ResponseEntity<?> getPaymentsByMonth(@PathVariable UUID userId){
        List<Salary> salaryList = salaryService.getPaymentsByUser(userId);
        return ResponseEntity.ok(salaryList);
    }
}
