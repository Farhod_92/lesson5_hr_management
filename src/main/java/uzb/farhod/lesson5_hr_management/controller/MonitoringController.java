package uzb.farhod.lesson5_hr_management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson5_hr_management.entity.Task;
import uzb.farhod.lesson5_hr_management.entity.Turniket;
import uzb.farhod.lesson5_hr_management.entity.User;
import uzb.farhod.lesson5_hr_management.payload.MonitoringDto;
import uzb.farhod.lesson5_hr_management.service.MonitoringService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@PreAuthorize(value = "hasAnyRole('ROLE_DIRECTOR', 'ROLE_HR_MANAGER')")
@RequestMapping("/api/monitoring")
public class MonitoringController {

    @Autowired
    private MonitoringService monitoringService;

    @GetMapping("/users")
    public ResponseEntity<?> getUsers(){
        List<User> users = monitoringService.getUsers();
        return ResponseEntity.ok(users);
    }

    //xodimning belgilangan oraliq vaqt bo’yicha ishga kelib-ketishi va bajargan tasklari haqida ma’lumot
    @GetMapping("/turniketAndTask")
    public ResponseEntity<?> getTurniketAndTaskList(@Valid @RequestBody MonitoringDto monitoringDto){
        Map<String, Object> map = monitoringService.getTurniketAndTaskList(monitoringDto);
        return ResponseEntity.ok(map);
    }

    //userning kechikib bajarilgan topshiriqlar
    @GetMapping("/taskControl/{userId}")
    public ResponseEntity<?> getExpiredTasks(@PathVariable UUID userId){
        Map<String,Object> taskMonitoringMap = monitoringService.getExpiredTasks(userId);
        return ResponseEntity.ok(taskMonitoringMap);
    }

}
