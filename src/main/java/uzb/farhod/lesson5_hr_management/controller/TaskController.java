package uzb.farhod.lesson5_hr_management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson5_hr_management.entity.Task;
import uzb.farhod.lesson5_hr_management.payload.ApiResponse;
import uzb.farhod.lesson5_hr_management.payload.TaskDto;
import uzb.farhod.lesson5_hr_management.service.TaskService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/api/task")
public class TaskController {
    @Autowired
    private TaskService taskService;

    @PreAuthorize(value = "hasAnyRole('ROLE_DIRECTOR','ROLE_HR_MANAGER','ROLE_MANAGER')")
    @PostMapping
    public ResponseEntity<?> addTask(@Valid @RequestBody TaskDto taskDto){
        ApiResponse apiResponse = taskService.addTask(taskDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping
    public ResponseEntity<?> getTasks(){
        List<Task> tasks = taskService.getTasks();
        return ResponseEntity.status(!tasks.isEmpty()?201:409).body(tasks);
    }

    //vazifa bajarilganini belgilab qoyish
    @PostMapping("/done/{id}")
    public ResponseEntity<?> doneTasks(@PathVariable UUID id){
        ApiResponse apiResponse = taskService.doneTask(id);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }


    //validatsiya message
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
