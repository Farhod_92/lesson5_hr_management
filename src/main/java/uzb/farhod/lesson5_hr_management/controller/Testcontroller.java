package uzb.farhod.lesson5_hr_management.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/test")
public class Testcontroller {

    @PreAuthorize(value = "hasRole('ROLE_DIRECTOR')")
    @GetMapping("/director")
    public String director(){
        return "DIRECTOR";
    }

    @PreAuthorize(value = "hasAnyRole('ROLE_DIRECTOR','ROLE_MANAGER')")
    @GetMapping("/manager")
    public String manager(){
        return "MANAGER";
    }

    @PreAuthorize(value = "hasAnyRole('ROLE_DIRECTOR','ROLE_MANAGER','ROLE_WORKER')")
    @GetMapping("/worker")
    public String worker(){
        return "WORKER";
    }
}
