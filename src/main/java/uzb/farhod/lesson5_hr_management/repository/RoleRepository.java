package uzb.farhod.lesson5_hr_management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson5_hr_management.entity.Role;
import uzb.farhod.lesson5_hr_management.entity.enums.RoleName;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(RoleName roleName);
}
