package uzb.farhod.lesson5_hr_management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uzb.farhod.lesson5_hr_management.entity.Task;
import uzb.farhod.lesson5_hr_management.entity.enums.TaskState;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

public interface TaskRepository extends JpaRepository<Task, UUID> {
    List<Task> findAllByUser_Id(UUID id);
    List<Task> findAllByStateAndUpdatedAtAfterAndUpdatedAtBeforeAndUser_Id(TaskState taskState,Timestamp fromTime, Timestamp untilTime, UUID id);
    List<Task> findAllByStateAndDoneInTimeAndUser_Id(TaskState state,boolean doneInTime, UUID id);
    List<Task> findAllByStateAndUser_Id(TaskState state, UUID id);
}
