package uzb.farhod.lesson5_hr_management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uzb.farhod.lesson5_hr_management.entity.Turniket;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

public interface TurniketRepository extends JpaRepository<Turniket, UUID> {
    List<Turniket> findAllByTimeAfterAndTimeBeforeAndUser_Id(Timestamp fromTime, Timestamp untilTime, UUID userId);
}
