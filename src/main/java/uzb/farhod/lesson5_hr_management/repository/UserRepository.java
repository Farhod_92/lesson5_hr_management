package uzb.farhod.lesson5_hr_management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson5_hr_management.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    boolean existsByEmail(String email);
    Optional<User> findByEmailAndEmailCode(String email, String code);
    Optional<User> findByEmail(String email);
}
