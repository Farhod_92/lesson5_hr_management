package uzb.farhod.lesson5_hr_management.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uzb.farhod.lesson5_hr_management.entity.Salary;

import java.util.List;
import java.util.UUID;

public interface SalaryRepository extends JpaRepository<Salary, UUID> {
    @Query(value = "select * from salary  where " +
            "extract(year from payment_time) = :year and " +
            "extract(month from payment_time)= :month", nativeQuery = true)
    List<Salary> getPaymentsToMonth(@Param("year") int year,@Param("month") int month);
    List<Salary> findAllByUser_Id(UUID userId);
}
