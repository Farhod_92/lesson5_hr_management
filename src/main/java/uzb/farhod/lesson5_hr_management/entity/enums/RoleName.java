package uzb.farhod.lesson5_hr_management.entity.enums;

import uzb.farhod.lesson5_hr_management.payload.ApiResponse;

public enum RoleName {
    ROLE_DIRECTOR,
    ROLE_HR_MANAGER,
    ROLE_MANAGER,
    ROLE_WORKER;

    public static RoleName getRoleName(String rolename){
        switch (rolename){
            case "ROLE_HR_MANAGER":
                return RoleName.ROLE_HR_MANAGER;
            case "ROLE_MANAGER":
               return RoleName.ROLE_MANAGER;
            case "ROLE_WORKER":
                return RoleName.ROLE_WORKER;
            default:
                return null;
        }
    }
}
