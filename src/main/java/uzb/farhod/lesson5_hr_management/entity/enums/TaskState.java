package uzb.farhod.lesson5_hr_management.entity.enums;

public enum TaskState {
    NEW,
    IN_PROCESS,
    DONE
}
