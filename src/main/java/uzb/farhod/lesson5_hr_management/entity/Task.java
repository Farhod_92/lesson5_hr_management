package uzb.farhod.lesson5_hr_management.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import uzb.farhod.lesson5_hr_management.entity.enums.TaskState;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Task {
    @Id
    @GeneratedValue
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TaskState state;

    @Column(nullable = false)
    private String name;

    private String annotation;

    private Date deadLine;//tugatish vaqti

    private Boolean doneInTime; //vazifa o'z vaqtida bajarilganini tekshisrish uchun

    @JsonIgnore
    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;

    //AUDIT
    @CreationTimestamp
    private Timestamp cratedAt;

    @CreationTimestamp
    private Timestamp updatedAt;

    @CreatedBy
    private UUID createdBy;

    @LastModifiedBy
    private UUID updatedBy;

}
