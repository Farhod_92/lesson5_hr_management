package uzb.farhod.lesson5_hr_management.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson5_hr_management.entity.Salary;
import uzb.farhod.lesson5_hr_management.entity.User;
import uzb.farhod.lesson5_hr_management.payload.ApiResponse;
import uzb.farhod.lesson5_hr_management.repository.SalaryRepository;
import uzb.farhod.lesson5_hr_management.repository.UserRepository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class SalaryService {
    @Autowired
    private SalaryRepository salaryRepository;

    @Autowired
    private UserRepository userRepository;

    public ApiResponse payment(UUID userId){
        Optional<User> optionalUser = userRepository.findById(userId);
        if(!optionalUser.isPresent())
            return new ApiResponse("user topilamdi",false);

        User user= optionalUser.get();
        Salary salary=new Salary();
        salary.setUser(user);
        salary.setAmount(user.getSalary());
        salaryRepository.save(salary);
        return new ApiResponse("to'lov saqlandi", true);
    }

    //oy bo'yicha berilgan oyliklarni ko'rish
    //2021-01 formatda oy keladi
    public List<Salary> getPaymentsByMonth(String time){
        String[] split = time.split("-");
        int year=Integer.parseInt(split[0]);
        int month=Integer.parseInt(split[1]);
        List<Salary> paymentsToMonth = salaryRepository.getPaymentsToMonth(year, month);
        return paymentsToMonth;
    }

    public List<Salary> getPaymentsByUser(UUID userId) {
       return salaryRepository.findAllByUser_Id(userId);
    }
}
