package uzb.farhod.lesson5_hr_management.service;

import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import uzb.farhod.lesson5_hr_management.entity.Role;
import uzb.farhod.lesson5_hr_management.entity.User;
import uzb.farhod.lesson5_hr_management.entity.enums.RoleName;
import uzb.farhod.lesson5_hr_management.payload.ApiResponse;
import uzb.farhod.lesson5_hr_management.payload.LoginDto;
import uzb.farhod.lesson5_hr_management.payload.RegisterDto;
import uzb.farhod.lesson5_hr_management.repository.RoleRepository;
import uzb.farhod.lesson5_hr_management.repository.UserRepository;
import uzb.farhod.lesson5_hr_management.security.JwtProvider;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping
    public ApiResponse register(@RequestBody RegisterDto registerDto){
        boolean existsByEmail = userRepository.existsByEmail(registerDto.getEmail());
        if(existsByEmail)
            return new ApiResponse("ushbu email mavjud",false);

        //TODO: Rolenameda metod ochish
        //yangi qo'shilayotgan xodim rolini aniqlash
        RoleName newUserRoleName;
        switch (registerDto.getRoleName()){
            case "ROLE_HR_MANAGER":
                newUserRoleName=RoleName.ROLE_HR_MANAGER;
                break;
            case "ROLE_MANAGER":
                newUserRoleName=RoleName.ROLE_MANAGER;
                break;
            case "ROLE_WORKER":
                newUserRoleName=RoleName.ROLE_WORKER;
                break;
            default:
                return new ApiResponse("rol topilmadi",false);
        }

        //Tizimga kirgan user rolini aniqlash
        RoleName loggedUserRoleName=getLoggedUserRoleName();

        //HR_MANAGER WORKER rolli xodimni qo'shayotganini tekshirish
        //DIRECTOR xoxlagan rolli userni qo'sha oladi
        if(loggedUserRoleName.equals(RoleName.ROLE_HR_MANAGER) && !newUserRoleName.equals(RoleName.ROLE_WORKER))
            return new ApiResponse("siz faqat worker qo'sha olasiz",false);

        User user=new User();
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setPassword(passwordEncoder.encode("123"));//default password
        user.setRoles(Collections.singleton(roleRepository.findByName(newUserRoleName)));
        user.setEmailCode(UUID.randomUUID().toString());
        user.setSalary(registerDto.getSalary());

        boolean messageSent = sendEmail(user.getEmail(), user.getEmailCode());
        if(messageSent) {
            userRepository.save(user);
            return new ApiResponse("pochtangizga kirib ro'yxatdan o'tishni tsqdiqlang", true);
        }
        return new ApiResponse("regitratsiyada xatolik", false);
    }

    public boolean sendEmail(String sendingEmail, String code){
        try{
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom("F.x.1992@inbox.ru");
            message.setTo(sendingEmail);
            message.setSubject("PDP Akkauntingizni tasdiqlang");
            message.setText("http:/localhost:8080/api/auth/verifyEmail?email=" + sendingEmail + "&code=" + code+"&password=");
            javaMailSender.send(message);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public ApiResponse verifyEmail(String email, String code, String password) {
        Optional<User> optionalUser = userRepository.findByEmailAndEmailCode(email, code);
        if(!optionalUser.isPresent())
            return new ApiResponse("user tasdiqlanmadi", false);
        User user= optionalUser.get();
        user.setEnabled(true);
        user.setEmailCode(null);
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
        return new ApiResponse("user aktivlashtirildi", true);
    }

    public ApiResponse login(LoginDto loginDto){
        try{
            Authentication authentication =
                    authenticationManager.authenticate(
                            new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));
            User user = (User) authentication.getPrincipal();
            String token = jwtProvider.generateToken(user.getEmail(), user.getRoles());
            token = "Bearer " + token;
            return new ApiResponse(token, true);
        }catch (Exception e){
            return new ApiResponse("autentifikatsiyadan o'tmadingiz: -> :"+e.getMessage(), false);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByEmail(username);
        return optionalUser.orElseThrow(()->new UsernameNotFoundException(username+ " topilmadi!"));
    }

    public RoleName getLoggedUserRoleName(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if(authentication!=null
                && authentication.isAuthenticated()
                && !authentication.getPrincipal().equals("anonymousUser")){
            User loggedUser = (User)authentication.getPrincipal();
            Set<Role> roles = loggedUser.getRoles();

            for(Role r:roles){
                return  r.getName();
            }
        }
        return null;
    }

    @Bean
    public User getLoggedUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null
                && authentication.isAuthenticated()
                && !authentication.getPrincipal().equals("anonymousUser")) {
            User loggedUser = (User) authentication.getPrincipal();
            return  loggedUser;
        }
        return null;
    }
}
