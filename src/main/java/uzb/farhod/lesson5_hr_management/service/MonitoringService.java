package uzb.farhod.lesson5_hr_management.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson5_hr_management.entity.Task;
import uzb.farhod.lesson5_hr_management.entity.Turniket;
import uzb.farhod.lesson5_hr_management.entity.User;
import uzb.farhod.lesson5_hr_management.entity.enums.TaskState;
import uzb.farhod.lesson5_hr_management.payload.MonitoringDto;
import uzb.farhod.lesson5_hr_management.repository.TaskRepository;
import uzb.farhod.lesson5_hr_management.repository.TurniketRepository;
import uzb.farhod.lesson5_hr_management.repository.UserRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class MonitoringService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TurniketRepository turniketRepository;

    @Autowired
    private TaskRepository taskRepository;

    public List<User> getUsers(){
        return userRepository.findAll();
    }

    public  Map<String,Object> getTurniketAndTaskList(MonitoringDto monitoringDto) {
        Map<String,Object> monitoringList=new HashMap<>();

        List<Turniket> turniketList = turniketRepository.findAllByTimeAfterAndTimeBeforeAndUser_Id(
                monitoringDto.getFromTime(),
                monitoringDto.getUntilTime(),
                monitoringDto.getUserId());

        List<Task> taskList=taskRepository.findAllByStateAndUpdatedAtAfterAndUpdatedAtBeforeAndUser_Id(
                TaskState.DONE,
                monitoringDto.getFromTime(),
                monitoringDto.getUntilTime(),
                monitoringDto.getUserId()
        );
        monitoringList.put("turniketList",turniketList);
        monitoringList.put("taskList",taskList);
        return monitoringList;
    }

    //userning o'z vaqtida bajargan va bajarmagan topshiriqlarini ko'rish
    public Map<String,Object> getExpiredTasks(UUID userId){
        Map<String,Object> taskMonitoringMap=new HashMap<>();
        List<Task> onTimeTasks = taskRepository.findAllByStateAndDoneInTimeAndUser_Id(TaskState.DONE,true, userId);
        List<Task> expiredTasks = taskRepository.findAllByStateAndDoneInTimeAndUser_Id(TaskState.DONE,false, userId);
        List<Task> newTasks = taskRepository.findAllByStateAndUser_Id(TaskState.NEW, userId);
        List<Task> inProcessTasks = taskRepository.findAllByStateAndUser_Id(TaskState.IN_PROCESS, userId);
        taskMonitoringMap.put("onTimeTasks",onTimeTasks);
        taskMonitoringMap.put("expiredTasks",expiredTasks);
        taskMonitoringMap.put("newTasks",newTasks);
        taskMonitoringMap.put("inProcessTasks",inProcessTasks);
        return taskMonitoringMap;
    }
}
