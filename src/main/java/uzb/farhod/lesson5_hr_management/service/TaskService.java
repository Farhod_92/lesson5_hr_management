package uzb.farhod.lesson5_hr_management.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson5_hr_management.entity.Role;
import uzb.farhod.lesson5_hr_management.entity.Task;
import uzb.farhod.lesson5_hr_management.entity.User;
import uzb.farhod.lesson5_hr_management.entity.enums.RoleName;
import uzb.farhod.lesson5_hr_management.entity.enums.TaskState;
import uzb.farhod.lesson5_hr_management.payload.ApiResponse;
import uzb.farhod.lesson5_hr_management.payload.TaskDto;
import uzb.farhod.lesson5_hr_management.repository.TaskRepository;
import uzb.farhod.lesson5_hr_management.repository.UserRepository;

import java.util.*;

@Service
public class TaskService {
    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private AuthService authService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    public ApiResponse addTask(TaskDto taskDto) {
        Optional<User> optionalUser = userRepository.findById(taskDto.getUserId());
        if (!optionalUser.isPresent())
            return new ApiResponse("user topilmadi", false);

        //tizimga kirgan userni rolini aniqlash
        RoleName loggedUserRoleName = authService.getLoggedUserRoleName();

        //vazifa qo'yilayotgan userning rolini aniqlash
        User user = optionalUser.get();
        Set<Role> roles = user.getRoles();
        RoleName taskUserRoleName = null;
        for (Role r : roles) {
            taskUserRoleName = r.getName();
            break;
        }

        if (taskUserRoleName == null)
            return new ApiResponse("task user roli topilmadi", false);

        //HR_MANAGER yoki MANAGER DIRECTORga yoki MANAGERLARGA vazifa qo'ymayotganini tekshirish
        //DIRECTOR roli tekshirilmaydi
        if (loggedUserRoleName.equals(RoleName.ROLE_HR_MANAGER) || loggedUserRoleName.equals(RoleName.ROLE_MANAGER)) {
            if (taskUserRoleName.equals(RoleName.ROLE_DIRECTOR)
                    || taskUserRoleName.equals(RoleName.ROLE_HR_MANAGER)
                    || taskUserRoleName.equals(RoleName.ROLE_MANAGER)) {
                return new ApiResponse("siz faqat WORKERlarga vazifa qo'ya olasiz", false);
            }
        }

        Task task = new Task();
        task.setName(taskDto.getName());
        task.setAnnotation(taskDto.getAnnotation());
        task.setDeadLine(taskDto.getDeadLine());
        task.setUser(user);
        task.setState(TaskState.NEW);

        sendEmail(user.getEmail(),
                "Sizga yangi vazifa berildi",
                "vazifani ko'rish uchun shaxsiy kabinetingizga kiring");

        taskRepository.save(task);
        return new ApiResponse("task qo'shildi", true);
    }

    public List<Task> getTasks() {
        User loggedUser = authService.getLoggedUser();
        List<Task> tasks = taskRepository.findAllByUser_Id(loggedUser.getId());
            return tasks;
    }

    public ApiResponse doneTask(UUID taskId){
        User loggedUser = authService.getLoggedUser();
        Optional<Task> optionalTask = taskRepository.findById(taskId);
        if(!optionalTask.isPresent())
            return new ApiResponse("task topilmadi", false);

        Task task = optionalTask.get();

        if(!task.getUser().getId().equals(loggedUser.getId()))
            return new ApiResponse("bu vazifa sizga tegishli emas", false);

        Optional<User> optionalUser = userRepository.findById(task.getCreatedBy());
        if(!optionalUser.isPresent())
            return new ApiResponse("vazifa qo'yuvchi user topilmadi", false);

        User taskMaster = optionalUser.get();
        boolean sendEmail = sendEmail(
                taskMaster.getEmail(),
                loggedUser.getUsername() + " ga qo'ygan " + task.getName() + " vazifangiz bajarildi",
                "vazifani tekshirib ko'ring"
        );

        if(sendEmail) {
            task.setState(TaskState.DONE);
            //task o'z vaqtida bajarilganinin tekshirish
            if(task.getDeadLine().before(new Date()))
                task.setDoneInTime(true);
            else
                task.setDoneInTime(false);

            taskRepository.save(task);

            return new ApiResponse("o'zgartirsh saqlandi", true);
        }
        return new ApiResponse("mailga xabar yuborilmadi qaytadan urinib ko'ring", false);

    }

    public boolean sendEmail(String sendingEmail, String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom("F.x.1992@inbox.ru");
            message.setTo(sendingEmail);
            message.setSubject(subject);
            message.setText(text);
            javaMailSender.send(message);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }



}
