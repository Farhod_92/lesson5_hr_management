package uzb.farhod.lesson5_hr_management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson5HrManagementApplication {
    public static void main(String[] args) {
        SpringApplication.run(Lesson5HrManagementApplication.class, args);
    }
    //https://docs.google.com/document/d/1p39pWX1zDLWoRQcmY5InjwYM3p0s687OdPPmmyEW0c8/edit?usp=sharing
}
